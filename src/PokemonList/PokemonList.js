import React from 'react';
import './PokemonList.css';
import { Link } from 'react-router-dom';

export default class PokemonList extends React.Component {
  getList(list) {
    return list.map((item) => {
      const selectedClass = this.props.selectedId === item.id ? 'selected' : '';

      return (
        <Link
          to={`/${item.id}`}
          key={item.id}
          className={`pokemon-list-item ${selectedClass}`}
        >
          {item.name.english}
        </Link>
      );
    });
  }

  render() {
    const list = this.getList(this.props.list);

    return (
      <div className="pokemon-list">
        {list}
      </div>
    );
  }
}
