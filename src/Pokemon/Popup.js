import React from 'react';

export default class Popup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: this.props.data,
      newProps: []
    };
  }

  removeType(i) {
    // TODO: clone the data before changing it
    let data = this.state.data;
    data.type.splice(i, 1);
    this.setState({ data });
  }

  addType() {
    let data = this.state.data;
    data.type.push('');
    this.setState({ data });
  }

  addNewProperty() {
    const newProps = this.state.newProps.slice();
    this.setState({
      newProps: newProps.concat([{
        key: '',
        value: ''
      }])
    });
  }

  removeNewProp(i) {
    let newProps = this.state.newProps.slice();
    newProps.splice(i, 1);
    this.setState({ newProps });
  }

  save() {
    let data = this.state.data;
    data.type = data.type.filter((item) => {
      return item !== '';
    });

    const newProps = this.state.newProps;
    newProps.forEach((item) => {
      if (item.key) {
        data[item.key] = item.value;
      }
    });

    this.props.update(data);
    this.close();
  }

  close() {
    this.props.close();
  }

  handleNewPropChange(event, i, key) {
    let newProps = this.state.newProps.slice();
    newProps[i][key] = event.target.value;
    this.setState({ newProps });
  }

  handleChange(event, key, value) {
    let newValue = event.target.value;

    if (key === 'base') {
      newValue = +newValue;
    }

    let data = this.state.data;
    data[key][value] = newValue;
    this.setState({ data });
  }

  getTypeList(type) {
    return type.map((item, index) => {
      return (
        <div
          className="form-group popup-type-item"
          key={index}
        >
          <input
            type="text"
            value={item}
            className="form-control"
            onChange={(e) => { this.handleChange(e, 'type', index); }}
          />
          <button
            className="btn btn-secondary"
            onClick={() => { this.removeType(index); }}
          >X</button>
        </div>
      );
    });
  }

  getNewPropsList(newProps) {
    return newProps.map((item, index) => {
      return (
        <div
          className="new-property form-group"
          key={index}
        >
          <div>
            <label>
              Key
              <input
                className="form-control"
                type="text"
                value={item.key}
                onChange={(e) => { this.handleNewPropChange(e, index, 'key'); }}
              />
            </label>
          </div>
          <div>
            <label>
              Value
              <input
                className="form-control"
                type="text"
                value={item.value}
                onChange={(e) => { this.handleNewPropChange(e, index, 'value'); }}
              />
            </label>
          </div>
          <div>
            <button
              className="btn btn-secondary"
              onClick={() => { this.removeNewProp(index); }}
            >Remove</button>
          </div>
        </div>
      );
    });
  }

  renderChildProperty(data, type, key1, key2) {
    return (
      <div className="popup-item form-group">
        <label>
          {`${key1} -> ${key2}`}
          <input
            className="form-control"
            type={type}
            value={data[key1][key2]}
            onChange={(e) => { this.handleChange(e, key1, key2); }}
          />
        </label>
      </div>
    );
  }

  renderNameProperties(data) {
    return (
      <div className="popup-item-wrapper">
        {this.renderChildProperty(data, 'text', 'name', 'english')}
        {this.renderChildProperty(data, 'text', 'name', 'japanese')}
        {this.renderChildProperty(data, 'text', 'name', 'chinese')}
      </div>
    );
  }

  renderBaseProperties(data) {
    return (
      <div className="popup-item-wrapper">
        {this.renderChildProperty(data, 'number', 'base', 'HP')}
        {this.renderChildProperty(data, 'number', 'base', 'Attack')}
        {this.renderChildProperty(data, 'number', 'base', 'Defense')}
        {this.renderChildProperty(data, 'number', 'base', 'Sp. Attack')}
        {this.renderChildProperty(data, 'number', 'base', 'Sp. Defense')}
        {this.renderChildProperty(data, 'number', 'base', 'Speed')}
      </div>
    );
  }

  renderType(type) {
    const typeList = this.getTypeList(type);

    return (
      <div className="popup-item-wrapper">
        <div className="popup-type-text">Type</div>
        <div>
          {typeList}
        </div>
        <div>
          <button
            className="btn btn-primary"
            onClick={this.addType.bind(this)}
          >Add Type</button>
        </div>
      </div>
    )
  }

  renderNewProps(newProps) {
    const newPropsList = this.getNewPropsList(newProps);

    return (
      <div className="popup-item-wrapper">
        <div>{newPropsList}</div>
        <div>
          <button
            className="btn btn-primary"
            onClick={this.addNewProperty.bind(this)}
          >Add New Property</button>
        </div>
      </div>
    );
  }

  renderOptions() {
    return (
      <div className="popup-item-wrapper popup-options">
        <button
          className="btn btn-success"
          onClick={this.save.bind(this)}
        >Save</button>
        <button
          className="btn btn-light"
          onClick={this.close.bind(this)}
        >Close</button>
      </div>
    );
  }

  render() {
    // TODO: clone objects before changing
    const data = this.state.data;

    return (
      <div
        className="popupWrapper"
        onClick={() => { this.close(); }}
      >
        <div
          className="popup"
          onClick={(e) => { e.stopPropagation(); }}
        >
          {this.renderNameProperties(data)}
          {this.renderType(data.type)}
          {this.renderBaseProperties(data)}
          {this.renderNewProps(this.state.newProps)}
          {this.renderOptions()}
        </div>
      </div>
    );
  }
}
