import React from 'react';
import './Pokemon.css';

export default class Pokemon extends React.Component {
  getTypeList(types) {
    return types.map((item, index) => {
      return (
        <div
          className="pokemon-type-item"
          key={index}
        >
          {item}
        </div>
      );
    })
  }

  defaultView() {
    return (
      <div className="default-text">No pokemon selected</div>
    );
  }

  render() {
    const pokemonData = this.props.data[0];

    if (!pokemonData) {
      return this.defaultView();
    }

    const typeList = this.getTypeList(pokemonData.type);

    return (
      <div>
        <div>
          <pre>
            {JSON.stringify(pokemonData, null, 2)}
          </pre>
        </div>
        <div>
          <button
            className="btn btn-primary"
            onClick={() => { this.props.openPopup(pokemonData.id); }}
          >Update</button>
        </div>
      </div>
    );

    return (
      <div className="pokemon">
        <div className="pokemon-name">{pokemonData.name.english}</div>
        <div className="pokemon-type">{typeList}</div>
        <div className="pokemon-damage">
          <div className="pokemon-attack">
            <span>Attack: </span>
            <span className="bold">{pokemonData.base.Attack}</span>
          </div>
          <div className="pokemon-defence">
            <span>Defense: </span>
            <span className="bold">{pokemonData.base.Defense}</span>
          </div>
        </div>
        <div>
          <button
            className="btn btn-primary"
            onClick={() => { this.props.openPopup(pokemonData.id); }}
          >Update</button>
        </div>
      </div>
    );
  }
}
