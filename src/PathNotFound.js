import React from 'react';

export default class PathNotFound extends React.Component {
  render() {
    return (
      <div>Path not found!</div>
    );
  }
}
