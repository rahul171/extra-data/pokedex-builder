import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Home from './Home';
import PathNotFound from './PathNotFound';

class App extends React.Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/:id" component={Home} />
          <Route component={PathNotFound} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
