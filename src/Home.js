import React from 'react';
import './Home.css';
import PokemonList from './PokemonList/PokemonList'
import Pokemon from './Pokemon/Pokemon'
import Popup from './Pokemon/Popup'
import data from './data/pokedex.json';

class Home extends React.Component {
  constructor(props) {
    super(props);

    let selectedId = null;

    if (this.props.match.params.id) {
      selectedId = +this.props.match.params.id;
    }

    this.state = {
      data,
      selectedId,
      showPopup: false,
      updateId: null
    }
  }

  componentDidUpdate(prevProps) {
    const newId = this.props.match.params.id;

    if (newId !== prevProps.match.params.id) {
      this.selectPokemon(+newId);
    }
  }

  selectPokemon(id) {
    this.setState({ selectedId: id });
  }

  togglePopup(id) {
    this.setState({
      showPopup: !this.state.showPopup,
      updateId: id
    });
  }

  openPopup(id) {
    this.togglePopup(id);
  }

  closePopup() {
    this.togglePopup();
  }

  updatePokemon(pokemon) {
    if (!pokemon.name.english) {
      return false;
    }

    let redirect = false;
    let data = this.state.data;
    const index = data.findIndex((item) => {
      return item.id === pokemon.id;
    });

    if (index !== -1) {
      data[index] = pokemon;
    } else {
      data.push(pokemon);
      redirect = true;
    }

    this.setState({ data });

    if (redirect) {
      this.props.history.push('/' + pokemon.id);
    }
  }

  getDefaultData() {
    return {
      name: {
        english: '',
        japanese: '',
        chinese: ''
      },
      type: [],
      base: {
        HP: '',
        Attack: '',
        Defense: '',
        'Sp. Attack': '',
        'Sp. Defense': '',
        Speed: ''
      }
    };
  }

  renderPopup(data) {
    const showPopup = this.state.showPopup;
    let popupData;

    if (showPopup) {
      popupData = data.filter((item) => {
        return item.id === this.state.updateId;
      });

      popupData = popupData[0];
    }

    if (!popupData) {
      popupData = this.getDefaultData();
      popupData.id = data[data.length - 1].id + 1;
    }

    return (
      <div>
        {showPopup &&
          <Popup
            data={popupData}
            update={(data) => { this.updatePokemon(data); }}
            close={this.closePopup.bind(this)}
          />
        }
      </div>
    );
  }

  render() {
    const data = this.state.data,
      selectedId = this.state.selectedId;

    const pokemonData = data.filter((item) => {
      return item.id === selectedId;
    });

    return (
      <div className="app">
        <div className="header">
          <div className="headerItem">
            <button
              className="btn btn-primary"
              onClick={this.openPopup.bind(this)}
            >Add</button>
          </div>
        </div>
        <div className="main">
          <div className="pokemon-list-wrapper">
            <PokemonList
              list={data}
              selectedId={selectedId}
            />
          </div>
          <div className="pokemon-wrapper">
            <Pokemon
              data={pokemonData}
              openPopup={(id) => { this.openPopup(id); }}
            />
          </div>
        </div>
        {this.renderPopup(data)}
      </div>
    );
  }
}

export default Home;
